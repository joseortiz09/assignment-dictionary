%include "lib.inc"

global find_word

find_word:
    .loop:
        push rdi
        push rsi
        add rdi, 8          ; key address
        call string_equals
        test rax, rax		; If 0 in rax means not found
        jnz .found
        pop rsi
        pop rdi
        mov rdi, [rdi]      ; Jump to next block
        test rdi, rdi		; Check if next node exist
        jz .end
        jmp .loop
    .end:
        xor rax, rax
        ret
    .found:
        pop rdi             ; Put key in rdi
        call string_length  ; Calculate key size
        pop rdi
        add rax, rdi
        add rax, 9          ; Block value
        ret
