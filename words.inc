%include "colon.inc"

section .data

colon "word 5", eleven_word
db "explaining word5!", 0

colon "six word with spaces", sixth_word
db "explaining sixth word!", 0

colon "fourth", fourth_word
db "fourth word", 0

colon "third", third_word
db "third word explanation", 0

colon "second", second_word
db "second word explanation", 0

colon "first", first_word
db "first word explanation", 0