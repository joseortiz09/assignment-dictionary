%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define buff_size 256

global _start

section .data
buffer: times buff_size db 0

section .rodata

input_msg: db 'Enter the keyword: ', 0
empty_key_msg: db 'ERROR: String can not be empty', 0
buffer_overflow_msg: db 'ERROR: String should have less than 255 characters', 0
not_found_msg: db 'Keyword not found', 0

section .text

_start:
    call print_newline
    mov rdi, input_msg
    call print_out
    mov rdi, buffer
    mov rsi, buff_size
    call read_string
    test rax, rax
    je .empty_string_err         ; if test returns zero so print error empty_key_msg
    js .buffer_overflow_err      ; if function was not completed means buffer error, print the overflow error
    mov rdi, buffer
    call trim_string
    mov rdi, next
    mov rsi, buffer
    call find_word
    test rax, rax               ; if in rax 0, not found keyword
    je .not_found_err
    mov rdi, rax                ; otherwise, a pointer to a value in the dictionary in rax
    call print_out
    call print_newline
    xor rdi, rdi
    jmp _start

    .empty_string_err:
        mov rdi, empty_key_msg
        jmp .err

    .buffer_overflow_err:
        mov rdi, buffer_overflow_msg
        jmp .err
    .not_found_err:
        mov rdi, not_found_msg
    .err:
        call print_err
        jmp _start
