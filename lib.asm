section .text

global exit
global string_length
global print_string
global print_out
global print_err
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_expression
global parse_uint
global parse_int
global string_copy
global trim_string
global read_string

%define STDOUT 1
%define STDERR 2


; Принимает код возврата и завершает текущий процесс [DONE]
exit:
    mov rax, 60 ; Recording the system call number
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину [DONE]
string_length:
    push rdi
    mov rax, 0
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        pop rdi
        ret

print_out:
    mov rsi, STDOUT
    jmp print_string

print_err:
    mov rsi, STDERR

; Принимает указатель на нуль-терминированную строку, выводит её в stdout [DONE]
print_string:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, 1 ; system write
    syscall
    ret



; Принимает код символа и выводит его в stdout [DONE]
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1 ; system write
    mov rdx, 1 ; stdout
    mov rdi, 1
    syscall
    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA) [DONE]
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате  [DONE]
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r9, 10
    mov rax, rdi
    mov r10, rsp
    dec rsp
    .loop:
        xor rdx, rdx
        div r9
        mov rdi,rdx
        add rdi, '0'
        dec rsp
        mov byte [rsp], dil
        cmp rax, 0
        je .print
        jmp .loop
    .print:
        mov rdi, rsp
        push rdi
        call print_out
        pop rdi
        mov rsp, r10
    ret

; Выводит знаковое 8-байтовое число в десятичном формате [DONE]
print_int:
    cmp rdi, 0
    jge .no_sign_print
    neg rdi
    mov r8, rdi
    mov rdi, '-'
    call print_char
    mov rdi, r8
    call print_uint
    ret
    .no_sign_print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе [DONE]
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .not_equal
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
    .not_equal:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока [DONE]
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
	xor r14, r14
	.loop:
		cmp r13, r14
		je .error
		mov byte[r12+r14], al
		cmp al, 0x0
		je .end
		cmp al, 0x20
		je .end
		cmp al, 0x9
		je .end
		cmp al, 0xA
		je .end
		inc r14
		call read_char
		jmp .loop
	.end:
		mov byte[r12+r14], 0
		mov rax, r12
		mov rdx, r14
		pop r14
		pop r13
		pop r12
		ret
	.error:
		xor rax, rax
		pop r14
		pop r13
		pop r12
	ret

; Params: adress start buffer, size buffer
; Read line from stdin
; returns -1 if string is bigger for buffer (overflow)
; returns string in rax if is ok
read_string:
    xor r8, r8
    .loop:
        cmp rsi, r8
        je .err
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        cmp rax, 0x0
        je .end
        cmp rax, 0xA
        je .end
        mov [rdi+r8], al
        inc r8
        jmp .loop
    .err:
        mov r8, -0x1
    .end:
        mov [rdi+r8], byte 0x0
        mov rax, r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, 0xA
    .loop:
        mov r9b, [rdi+r8]
        cmp r9b, 0x30
        jb .end
        cmp r9b, 0x39
        ja .end
        sub r9b, 0x30
        mul r10
        add rax, r9
        inc r8
        jmp .loop
    .end:
        mov rdx, r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte [rdi], '-'
	je .is_negative
	call parse_uint
	ret
	.is_negative:
		inc rdi
		call parse_uint
		neg rax
		test rdx, rdx
		jz .end
		inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .overflow
    mov rcx, 0 ; current symbol
    .loop:
        mov r9, [rdi+rcx]
        mov [rsi+rcx], r9
        cmp rcx, rax
        je .end
        inc rcx
        jmp .loop
    .overflow:
        xor rax, rax
        ret
    .end:
        ret

; Accepts a pointer to a null-terminated string
; Removes leading and trailing spaces
trim_string:
    xor rax, rax
    mov rsi, rdi
    dec rsi
    .skip:
        inc rsi
        cmp byte[rsi], 0x20
        je .skip
        cmp rsi, rdi
        je .end
    .loop:
        mov r8b, [rsi+rax]
        mov [rdi+rax], r8b
        test r8b, r8b
        je .end
        inc rax
        jmp .loop
    .end:
        ret